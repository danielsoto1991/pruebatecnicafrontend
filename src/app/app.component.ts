import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Empleados cidenet';

  constructor(private router:Router){}

  listEmployee() {
    this.router.navigate(["list"]);
  }

  createEmployee(){
    this.router.navigate(["create"]);
  }
}



