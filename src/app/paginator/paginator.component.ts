import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  SimpleChanges,
  OnChanges
} from "@angular/core";

@Component({
  selector: "app-paginator",
  templateUrl: "./paginator.component.html",
  styleUrls: ["./paginator.component.css"]
})
export class PaginatorComponent implements OnInit, OnChanges {
  @Input() length: number = 0;
  @Input() pageSize: number = 0;
  @Input() pageIndex: number = 0;
  totalRegistersPaginator: number = 0;

  @Output() page = new EventEmitter();

  pageSizeOptions: number[] = [10, 20, 50];
  registersPerPage: any[] = [];
  paginatorRegistersNumber: string;

  constructor() {}

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.length) {
      this.loadDataPaginator();
    }
  }

  changePage() {
    this.page.emit({ pageIndex: this.pageIndex, pageSize: this.pageSize });
  }

  changePagePaginator(action: string) {
    if (action == "previous" && this.pageIndex > 0) {
      this.pageIndex = this.pageIndex - 1;
      this.loadDataPaginator();
    }
    if (
      action == "next" &&
      this.registersPerPage[this.pageIndex].final < this.length
    ) {
      this.pageIndex = this.pageIndex + 1;
      this.loadDataPaginator();
    }
  }

  loadDataPaginator() {
    this.registersPerPage = [];

    if (this.length < this.pageSize) {
      this.registersPerPage.length = 1;

      if (this.length === 0) {
        this.registersPerPage[0] = {
          initial: this.length,
          final: this.length
        };
      } else {
        this.registersPerPage[0] = {
          initial: this.length - (this.length - 1),
          final: this.length
        };
      }

      this.paginatorRegistersNumber =
        this.registersPerPage[0].initial +
        " - " +
        this.registersPerPage[0].final +
        " de " +
        this.length;
    } else {
      let numberOfPages: number = Math.ceil(this.length / this.pageSize);
      this.registersPerPage.length = numberOfPages;

      let count: number = 0;

      this.totalRegistersPaginator = this.length;

      for (let i = 0; i < this.registersPerPage.length; i++) {
        this.totalRegistersPaginator =
          this.totalRegistersPaginator - this.pageSize;

        if (this.totalRegistersPaginator >= 0) {
          count = count + this.pageSize;

          this.registersPerPage[i] = {
            initial: count - (this.pageSize - 1),
            final: count
          };
        } else {
          let registers = this.pageSize + this.totalRegistersPaginator + count;
          this.registersPerPage[i] = {
            initial: count + 1,
            final: registers
          };
        }
      }
      this.paginatorRegistersNumber =
        this.registersPerPage[this.pageIndex].initial +
        " - " +
        this.registersPerPage[this.pageIndex].final +
        " de " +
        this.length;
    }
    this.changePage();
  }

  onChangeSelection(selected) {
    this.pageSize = parseInt(selected);
  }
}
