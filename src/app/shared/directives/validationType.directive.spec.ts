import { ValidationTypeDirective } from './validationType.directive';

describe('ValidationTypeDirective', () => {


  it('should create an instance', () => {
    const directive = new ValidationTypeDirective();
    expect(directive).toBeTruthy();
  });


  it('call onPaste successfull Alphanumeric', () => {

    const e: any = {
      clipboardData: {
        getData: (key: string): string => {
          return 'aaaa   asd';
        }

      }

    }

    const directive = new ValidationTypeDirective();
    directive.typeValidation = 'Alphanumeric';

    directive.onPaste(e);

    expect(directive).toBeTruthy();
  });

  it('call onPaste successfull Numeric', () => {

    const e: any = {
      clipboardData: {
        getData: (key: string): string => {
          return '12345';
        },
        preventDefault: () => {
          
        }

      }

    }

    const directive = new ValidationTypeDirective();
    directive.typeValidation = 'Numeric';

    directive.onPaste(e);

    expect(directive).toBeTruthy();
  });



});
