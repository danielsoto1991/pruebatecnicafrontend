import { Directive, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appValidationType]'
})
export class ValidationTypeDirective {

  @Input('appValidationType') typeValidation: string;

  constructor() { }

  @HostListener("keypress", ["$event"])
  onKeyDown(e: KeyboardEvent) {
    if (this.validate(e.keyCode)) {
      e.preventDefault();
    }
  }

  @HostListener("paste", ["$event"])
  onPaste(e: any) {
    const copyData: string = e.clipboardData.getData('text/plain');
    const data: string[] = copyData.split('');
    let cont = true;
    var i: number;
    for (i = 0; i < data.length && cont; i++) {
      if (this.validate(data[i].charCodeAt(0))) {
        cont = false;
        e.preventDefault();
      }
    }
  }

  validate(keyCode: number): boolean {
    let error: boolean = false;
    if (this.typeValidation === 'Alphanumeric') {
      error = this.validateSpecialChar(keyCode);
    }else if (this.typeValidation === 'Numeric'){
      error = this.validateNumberChar(keyCode);
    }
    return error;
  }

  validateNumberChar(keyCode: number): boolean {
    let error: boolean = false;
    if ((keyCode < 48 || keyCode > 57)) {
      error = true;
    }
    return error;
  }

  validateSpecialChar(keyCode: number): boolean { 
    let error: boolean = false;
    if ((keyCode >= 33 && keyCode <= 43) ||
      (keyCode >= 58 && keyCode <= 63) ||
      (keyCode == 91) || (keyCode == 93) || (keyCode == 94) ||
      (keyCode >= 123 && keyCode <= 126) ||
      (keyCode >= 161 && keyCode <= 191)) {
      error = true;
    }
    return error;
  }

}
