import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ValidationTypeDirective } from './validationType.directive';
import { FilterFieldsComponent } from '../filter-fields/filter-fields.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  declarations: [
    ValidationTypeDirective,
    FilterFieldsComponent
  ],
  exports:[
    ValidationTypeDirective,
    FilterFieldsComponent
  ]
})
export class DirectivesModule { }
