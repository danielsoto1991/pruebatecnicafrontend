import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterFieldsComponent } from './filter-fields.component';
import { ReactiveFormsModule } from '@angular/forms';

describe('FilterFieldsComponent', () => {
  let component: FilterFieldsComponent;
  let fixture: ComponentFixture<FilterFieldsComponent>;

  const filterObject: any = {
    filterType: 'BIN',
    filterValue: '5'
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ],
      declarations: [
        FilterFieldsComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#ngOnInit', () => {

    it('Should call the initialiaze', () => {

      spyOn(component, 'generateFilterForm');

      component.ngOnInit();

      expect(component.generateFilterForm).toHaveBeenCalled();
      expect(component.submittedFilter).toBeFalsy();

    });

  });

  describe('#generateFilterForm', () => {

    it('must generate the form of filter', () => {

      component.generateFilterForm();

      expect(component.filterForm).toBeDefined();

    });

  });

  describe('#filter', () => {

    it('must call method filter pass by parameter', () => {

      spyOn(component.filterMethod, 'emit');

      component.generateFilterForm();
      component.filterForm.patchValue(filterObject);
      component.filter();

      expect(component.filterMethod.emit).toHaveBeenCalled();
      expect(component.submittedFilter).toBeTruthy();

    });

  });

  describe('#deleteFilter', () => {

    it('must call method deletefilter pass by parameter', () => {

      spyOn(component, 'generateFilterForm');
      spyOn(component.deleteFilterMethod, 'emit');

      component.deleteFilter();

      expect(component.submittedFilter).toBeFalsy();
      expect(component.generateFilterForm).toHaveBeenCalled();
      expect(component.deleteFilterMethod.emit).toHaveBeenCalled();


    });

  });

});
