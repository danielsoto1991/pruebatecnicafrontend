import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-filter-fields',
  templateUrl: './filter-fields.component.html',
  styleUrls: ['./filter-fields.component.css']
})
export class FilterFieldsComponent implements OnInit {

  @Input() optionsFilter: string;
  @Output() filterMethod = new EventEmitter();
  @Output() deleteFilterMethod = new EventEmitter();


  filterForm: FormGroup;
  submittedFilter = false;
  validationMessages: { [key: string]: { [key: string]: string } };

  constructor(
    private formBuilder: FormBuilder
  ) {
    this.validationMessages = {
      filterType: {
        required: 'Debe seleccionar el campo del filtro'
      }
    }
   }

  ngOnInit() {
    this.generateFilterForm();
  }

  generateFilterForm(): any {
    this.filterForm = this.formBuilder.group({
      filterType: ['', Validators.required],
      filterValue: ['', Validators.required]
    });
  }

  get ff() { return this.filterForm.controls; }

  filter() {
    this.submittedFilter = true;
    if(this.filterForm.valid){
      const objectFilter: any = this.filterForm.getRawValue();
      this.filterMethod.emit(objectFilter);
    }

  }

  deleteFilter() {
    this.submittedFilter = false;
    this.generateFilterForm();
    this.deleteFilterMethod.emit();
  }

}

