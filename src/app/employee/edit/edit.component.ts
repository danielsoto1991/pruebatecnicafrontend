import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ServiceService } from 'src/app/service/service.service';
import { employee } from 'src/app/models/employee';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  employeeForm: FormGroup;
  employee: employee;
  date = new Date();
  yourDate = new Date();
  dateMin = this.yourDate.setDate(this.yourDate.getDate() - 30);
  today = this.date.getDate() + '/' + (this.date.getMonth() + 1) + '/' + this.date.getFullYear() + ' - ' + this.date.getHours() + ':' + this.date.getMinutes() + ':' + this.date.getSeconds();
  idTypes: String[] = ["Cédula de Ciudadanía", "Cédula de Extranjería", "Tarjeta de Identidad", "Pasaporte", "Carné diplomático"];
  jobsCountry: String[] = ["Colombia", "Estados Unidos"];
  areas: String[] = ["Administración", "Financiera", "Compras", "Infraestructura", "Operación", "Talento Humano", "Tecnología", "Servicios Varios"];
  states: String[] = ["Activo", "Inactivo"];
  emailAuto: String;
  validationMessages: { [key: string]: { [key: string]: string } };
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private service: ServiceService
  ) {
    this.validationMessages = {
      firstLastName: {
        required: "Debe digitar el primer apellido",
        pattern: "Solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ"
      },
      secondLastName: {
        required: "Debe digitar el segundo apellido",
        pattern: "Solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ"
      },
      firstName: {
        required: "Debe digitar el primer nombre",
        pattern: "Solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ"
      },
      otherNames: {
        pattern: "Solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ"
      },
      idNumber: {
        required: "Debe digitar un número de identificación",
        pattern: "No se permiten caracteres especiales"
      },
      admissionDate: {
        required: "Debe digitar una fecha de registro"
      },
      idType: {
        required: "Debe digitar una fecha de registro"
      },
      jobCountry: {
        required: "Debe digitar una fecha de registro"
      },
      area: {
        required: "Debe digitar una fecha de registro"
      }
    };
  }

  ngOnInit(): void {
    this.generateForm();
    this.getEmployee();
    // this.employeeForm.controls.documentNumber.setValue(member.documentNumber);
    // this.employeeForm.controls.name1.setValue(member.name);
  }

  get f() {
    return this.employeeForm.controls;
  }

  generateForm() {
    this.employeeForm = this.formBuilder.group({
      firstLastName: [
        "",
        Validators.compose([
          Validators.required, Validators.maxLength(20), Validators.pattern('[A-Z ]+')
        ])
      ],
      secondLastName: [
        "",
        Validators.compose([
          Validators.required, Validators.maxLength(20), Validators.pattern('[A-Z ]+')
        ])
      ],
      firstName: [
        "",
        Validators.compose([
          Validators.required, Validators.maxLength(20), Validators.pattern('[A-Z ]+')
        ])
      ],
      otherNames: [
        "",
        Validators.compose([
          Validators.maxLength(50), Validators.pattern('[A-Z ]+')
        ])
      ],
      idType: [
        "",
        Validators.compose([
          Validators.required
        ])
      ],
      idNumber: [
        "", Validators.compose([
          Validators.required, Validators.maxLength(20), Validators.pattern('[A-Za-z0-9]+')
        ])
      ],
      jobCountry: [
        "",
        Validators.compose([
          Validators.required
        ])
      ],
      email: new FormControl({
        value: "", disabled: true
      }, Validators.compose([
        Validators.required, Validators.maxLength(300)
      ])
      ),
      admissionDate: new FormControl({
        value: "", disabled: true
      }, Validators.required
      ),
      area: [
        "",
        Validators.compose([
          Validators.required
        ])
      ],
      state: [
        "", Validators.required
      ],
      creationDate: new FormControl({
        value: "", disabled: true
      }, Validators.required
      ),
      modificationDate: new FormControl({
        value: this.today, disabled: true
      }, Validators.required
      )
    });
  }

  getEmployee() {
    this.submitted = true;
    let numberId = localStorage.getItem("numberId");
    this.service.getEmpoyeeById(numberId).subscribe(data => {
      this.employee = data;
    })
  }

  editEmployee(employee:employee) {
    if (this.employeeForm.invalid) {
      return;
    }
    this.service.editEmployee(employee).subscribe(data => {
      this.employee=data;
      alert("Se ha actualizao el empleado exitosamente!");
      this.router.navigate(["list"]);
    },
      err => alert("El empleado ya existe!"));
  }

}




