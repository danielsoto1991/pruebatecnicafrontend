import { Component, OnInit } from '@angular/core';
import{ ServiceService } from '../../service/service.service';
import { Router } from '@angular/router';
import { employee } from 'src/app/models/employee';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  employeeDelete:employee;
  idTypes: String[] = ["Cédula de Ciudadanía", "Cédula de Extranjería", "Tarjeta de Identidad", "Pasaporte", "Carné diplomático"];
  jobsCountry: String[] = ["Colombia", "Estados Unidos"];
  areas: String[] = ["Administración", "Financiera", "Compras", "Infraestructura", "Operación", "Talento Humano", "Tecnología", "Servicios Varios"];
  employees: employee[];
  states: String[] = ["Activo", "Inactivo"];
  pageIndex: number = 0;
  totalCountPag: number;
  pageSize: number = 10;
  loadPaginator:boolean = false;
  constructor(
    private service:ServiceService, 
    private router:Router,
    private modal:NgbModal
    ) { }

  ngOnInit(): void {
    this.service.listEmployees().subscribe(data=>{
      this.employees=data;
    })
  }

  changePage($event) {
    this.service.listPageableEmployees($event.pageIndex, $event.pageSize);
  }

  editEmployee(employee:employee) {
    localStorage.setItem("numberId",employee.idNumber.toString());
    this.router.navigate(["edit"]);
  }

  copyEmployee(employee) {
    this.employeeDelete = employee;
  }

  deleteEmployee() {
    this.service.deleteEmployee(this.employeeDelete).subscribe(data=>{
      this.employees = this.employees.filter(e=>e!==this.employeeDelete);
      alert("Empleado eliminado!");
      this.router.navigate(["list"]);
    })
  }

}
