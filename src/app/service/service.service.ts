import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { employee } from '../models/employee';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http:HttpClient) { }

  url='http://localhost:9080/employee';

  listEmployees() {
    return this.http.get<employee[]>(this.url + '/list')
  }

  listPageableEmployees(page:number, size:number) {
    const options = {
			params: new HttpParams()
				.set('page', page.toString())
				.set('size', size.toString())
		}
    return this.http.get<employee[]>(this.url + '/list-paginate', options)
  }

  createEmployee(employee:employee) {
    return this.http.post<employee>(this.url + '/create', employee)
  }

  getEmpoyeeById(idNumber:string){
    const options = {
      params: new HttpParams()
          .set('idNumber', idNumber)
  }
    return this.http.get<employee>(this.url + '/get', options)
  }

  editEmployee(employee:employee) {
    return this.http.post<employee>(this.url + '/save', employee)
  }

  deleteEmployee(employee:employee) {
    const options = {
			params: new HttpParams()
				.set('idNumber', employee.idNumber.toString())
		}
    return this.http.get<string>(this.url + '/delete', options)
  }

}
