export class employee {
    id:String;
    idNumber:String;
	idType: String;
    firstLastName:String ;
    secondLastName:String ;
    firstName:String ;
    otherNames:String ;
    jobCountry:String ;
    email:String ;
    admissionDate:String ;
    area:String ;
    state:String ;
    creationDate:String ;
    modificationDate:String ;
}